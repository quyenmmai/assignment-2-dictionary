global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global print_error
global string_equals
global read_char
global read_word
global read_line
global parse_uint
global parse_int
global string_copy
section .text
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60 ;system call code entry
    syscall ;system call
    ret

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax,rax ;i=0
    .loop: ;while(str[i]!=0) i++
        cmp byte[rax+rdi],0 ;if end -> jump to .end
        je .end
        inc rax ;i++
        jmp .loop
    .end:
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    xor rax, rax
    push rdi
    call string_length
    pop rdi
    mov rdx, rax ;rdx = len(str)
    mov rsi, rdi ;rsi = line address
    mov rdi, 1 ;descriptor stdout
    mov rax, 1 ;setting system call number
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char: 
    push rdi ;put the character code in memory, because the system call takes the address of the character in rsi
    mov rdx, 1     
    mov rsi, rsp ;get the address of the character through the pointer to the stack
    pop rdi
    mov rax, 1      ; System call code "Output"
    mov rdi, 1      ; descriptor stdout
    syscall         ;System call
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    xor rax, rax
    mov rdi, '\n' ;load newline character and call print_char
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    xor rax, rax 
    mov r8, 10 ;foundation of the system
    mov r9, rsp ;store pointer to stack
    mov rax, rdi ;rax = N
    ;we will go from the end and write characters to the stack sequentially. and then stupidly call print_string
    push 0 ;last character '0', end of line
    .loop:
        xor rdx,rdx ;data register
        div r8 ;integer part in rax, remainder in rdx
        add rdx, '0' ;convert character to ASCII
        dec rsp ;go in the opposite direction from the end of the stack. push and pop do the same BUT step != 1
        mov byte[rsp], dl ;write character from rdx to address rs
        cmp rax,0 ;if the end we go ou
        je .end
        jmp .loop ;otherwise must go on

    .end:
        mov rdi, rsp ;write the address of the first character and call print_string
        push r9
        call print_string
        pop r9
        mov rsp, r9 ;restore stack ptr
        ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    xor rax, rax
    cmp rdi,0
    jl .neg ;if n<0 go to .neg
    jmp print_uint ;if >=0 then just output as unsigned

    .neg
        push rdi  ;store number
        mov rdi, '-' ; output -
        call print_char
        pop rdi
        neg rdi ;take a negative number
        jmp print_uint ;output the "modulus" of the number

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax
    xor rcx, rcx ;i
    xor r8, r8 ;clear all needed registers
    xor r9, r9
    .loop: ;r8b <- запись 8 bit char
        mov r8b, byte[rdi+rcx] ;r8 = s1[i]
        mov r9b, byte[rsi+rcx] ;r9 = s2[i]
        cmp r8b,r9b 
        jne .no ;if s1[i]!=s2[i] return  false
        cmp r8b,0
        je .yes ;if s1[i]==s2[i]=='0' return true;i.e. if all characters matched
        inc rcx ;i++
        jmp .loop
    
    .yes:
        mov rax, 1   ;return true
        ret
    .no:
        mov rax,0 ;return false
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax ;rax=0 -> input. stdin descriptor
    xor rdi,rdi 
    mov rdx, 1 ;len = 1
    push 0 ;put an empty character on the stack, now it is at [rsp]
    mov rsi, rsp ;load symbol address
    syscall
    pop rax ;load symbol from stack in rax
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    xor rcx, rcx
    xor rax, rax
    ;rdi -start address rsi - buffer size
    .loop:
        push rcx
        push rsi
        push rdi
        call read_char ;read symbol
        pop rdi
        pop rsi
        pop rcx

        cmp rax, 0 ;if end, jump to .end
        je .end

        cmp rax, ' ' ;checks for empty characters, jump to .skip_space
        je .skip_whitespace
        cmp rax, '	'
        je .skip_whitespace
        cmp rax, '\n'
        je .skip_whitespace

        mov [rdi+rcx], rax  ;we put the loaded symbol at the address. word[i]=char
        inc rcx ;i++
        cmp rcx, rsi ;if(i>buffer_length) overflow error
        jge .error

        jmp .loop

    .skip_whitespace:
        cmp rcx,0 ;if the word hasnot started yet, then skip.
        je .loop
        jmp .end ;and if there were already characters, then this space is the end of the word and leaving.
    .error:                                                                
        xor rax, rax ;return 0                               
        xor rdx, rdx
        ret
    .end:
        xor rax, rax
        mov [rdi+rcx], rax ;last null. s[i+1] = '0'
        mov rax, rdi ;addr
        mov rdx, rcx ;len
        ret
    




; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rcx, rcx
    mov r8, 10 ;foundation of the system
    .loop: ;movzx - extend with 0
        movzx r9, byte[rdi+rcx] ;r9 = s[i]
        cmp r9,0 ;if r9==0 end
        je .end
        cmp r9b, '0' ;checking that it is a digit. that is, the code is less than 9 and greater than 0.
        jl .end     ; if not a digit, we do not parse further.
        cmp r9b, '9'
        jg .end

        mul r8 
        sub r9b, '0' ; get a digit by subtracting code 0 from the character code. (ascii)
        add rax, r9 ;rax = rax*10 + digit. that is, shift by a row and add a number
        inc rcx ;i++
        jmp .loop

    .end
        mov rdx, rcx ;write the length to rdx. rdx = i
        ret


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    xor rdx, rdx ;clean registers
    mov rcx, rdi 
    cmp byte[rcx], '-';if s[i]== "-" switch to .neg
    je .neg
    jmp .pos ;otherwise go to .pos
    .neg:
        inc rcx ;i+=1, to remove minus
        mov rdi, rcx
        push rcx
        call parse_uint ;read unsigned, "modul"
        pop rcx
        neg rax ;take the negative
        inc rdx ;rdx = true, everything is ok
        ret
    .pos:
        mov rdi, rcx
        jmp parse_uint
        

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0

string_copy: ;args: rdi, rsi, rdx
    xor rax, rax
    xor rcx, rcx ;i=0
    push rsi
    push rdi
    push rcx
    push rdx
    call string_length
    pop rdx
    pop rcx
    pop rdi
    pop rsi

    mov r8, rax ;r8=len(str)
    cmp rdx, r8 ;if len(buffer)<len(string) it won't fit
    jl .error
    .loop:
        cmp rcx, r8;if(i> len(str)) break;
        jg .end
        mov r10,[rdi+rcx] ;buffer[i]=string[i]
        mov [rsi+rcx], r10
        inc rcx; i++
        jmp .loop
    
    .error:
        mov rax, 0 ;return 0
        ret
    .end:
        mov rax, r8 ;return length
        ret
