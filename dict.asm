%include "lib.inc"

section .text

global find_word


find_word:
    .loop:
        push rdi
        push rsi

        add rsi, 8
        call string_equals

        test rax, rax
        pop rsi
        pop rdi
        jne .right
        mov rsi
        test rsi, rsi
        je .error
        jmp .loop
    .error:
        xor rax, rax
        ret
    .right:
        mov rax, rsi
        ret
    
