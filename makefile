ASM=nasm
ASMFLAGS=-felf64
LD=ld

%.o: %.asm
	$(ASM) $(ASMFLAGS) $^

main: main.o dict.o lib.o dict.inc lib.inc colon.inc words.inc
	$(LD) -o $@ $^
main.o: main.asm dict.inc lib.inc colon.inc words.inc
	$(ASM) $(ASMFLAGS) $<

.PHONY: clean
clean:
	rm -f *.o
